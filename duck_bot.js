const Discord = require('discord.js');
const client = new Discord.Client();
const fs = require('fs');
const token = JSON.parse(fs.readFileSync('token.json', 'utf8'));

var quacks = ['duck_quack1.mp3', 'pissed_off_duck.mp3','duck.wav', 'mallard.mp3', 'QUACK.WAV', 'duck-quack3.wav', 'quack_quack.mp3'];

client.on('ready', () => {
    console.log('I am ready!');
});

client.on('message', message => {
    if (message.content === 'ping') {
        message.reply('pong');
    } else if (message.content === 'duck quack') {
        var voiceChannel = message.member.voiceChannel;
        voiceChannel.join().then(connection => {
            console.log('Connected!');
            var quack = './assets/';
            quack = quack + quacks[Math.floor(Math.random()*quacks.length)];
            console.log(quack);
            const dispatcher = connection.playFile(quack);
            dispatcher.on("end", end => {
                voiceChannel.leave();
            })
        }).catch(console.error);
    }
});

client.login(token.token);